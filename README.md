# SimpleOpenTapGui

Simple start/stop test plan application for OpenTAP test automation solution

### What it's for ###
The purpose of this project is to demonstrate how to make a simple GUI around OpenTAP test plan execution.
This is applicable for operator use in production, whose main concern is running a test plan for a particular product.

### What it does ###
The application allows the user to select a test plan, start the plan, and abort the test plan. It also creates a session log in the current working directory. It will show the current status of the test plan run, including whether or not a test plan has been selected, not yet run, or completed without verdict. If the test plan does have a verdict determined, it will display this instead upon completion, including Pass, Fail, Aborted, Error, etc.

### What it doesn't have ###
The application currently doesn't have much in the way of error handling, such as if the test plan fails. Detailed information is contained in the session log. (Some text editors, such as [Microsoft Visual Studio Code](https://code.visualstudio.com/ "VS Code"), will continually update a text file display as it is written to on disk, meaning near-real-time log viewing.)

### How to use it ###
It's recommended to either use this project as an example or build it on it to encompass your own needs. If you're just going to build and use it, or improve on it and then use it, you'll need to either launch from the build output or place the built exe in the %TAP_PATH% folder along with tap.exe. 

### Technical Info ###
SimpleOpenTapGui is a WPF (Windows Presentation Foundation) application targeted for .NET Framework 4.7.2, written in C# (language version 9.0). It uses the OpenTAP Nuget package. 

### About OpenTAP ###
More information about OpenTAP, including installation, documentation, and the forum can be found at the OpenTAP website: [OpenTAP](https://www.opentap.io/ "About OpenTAP")
The OpenTAP project can be found here: [OpenTAP GitLab](https://gitlab.com/OpenTAP/opentap "OpenTAP GitLab project")
The test plan execution is heavily based on this example: [RunTestPlan.Api](https://gitlab.com/OpenTAP/opentap/-/tree/master/sdk/Examples/TestPlanExecution/RunTestPlan.Api "Source Code")

### Screenshots ###

#### Beginning Screen ####
![beginning_screen](./Screenshots/beginning_screen.jpg)

#### Test Plan Selected ####
![test_plan_selected](./Screenshots/test_plan_selected.jpg)

#### Test Plan Completed (without verdict) ####
![completed_test_plan](./Screenshots/completed_test_plan.jpg)