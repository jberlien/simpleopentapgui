﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using OpenTap;

namespace SimpleOpenTapGUI.ViewModels
{
    /// <summary>
    /// ViewModel for main window, following MVVM design pattern
    /// </summary>
    internal class MainViewModel : INotifyPropertyChanged
    {
        #region Properties
        private bool _isRunning = false;
        /// <summary>
        /// Indicates whether test plan is currently running
        /// </summary>
        public bool IsRunning
        {
            get => _isRunning;
            set
            {
                _isRunning = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(NotRunning));
            }
        }

        /// <summary>
        /// Indicates that test plan is not currently running
        /// </summary>
        public bool NotRunning => !IsRunning;

        private string _tapPlanFilePath;
        /// <summary>
        /// The file path to the test plan to be run
        /// </summary>
        public string TapPlanFilePath
        {
            get => _tapPlanFilePath;
            set
            {
                _tapPlanFilePath = value;
                TestRunStatus = !string.IsNullOrEmpty(value) ? NotRunningStatus : NotSelectedStatus;
                try
                {
                    _testPlan = TestPlan.Load(value);
                    TestPlanName = _testPlan.Name;
                }
                catch
                {
                    TestPlanName = NotSelectedName;
                }
                OnPropertyChanged();
            }
        }

        #region Available Status Strings
        private const string NotRunningStatus = "Not Running";
        private const string NotSelectedStatus = "Not Selected";
        private const string CompletedStatus = "Completed";
        private const string RunningStatus = "Running";
        private const string PassedStatus = "Passed";
        private const string AbortedStatus = "Aborted";
        private const string FailedStatus = "Failed";
        private const string InconclusiveStatus = "Inconclusive";
        private const string ErrorStatus = "Error (see log for details)";
        #endregion

        private string _testRunStatus = NotSelectedStatus;
        /// <summary>
        /// Status of current test plan run
        /// </summary>
        public string TestRunStatus
        {
            get => _testRunStatus;
            set
            {
                _testRunStatus = value;
                OnPropertyChanged();
            }
        }

        private const string NotSelectedName = "[Not Selected]";

        private string _testPlanName = NotSelectedName;
        /// <summary>
        /// Name of currently loaded test plan
        /// </summary>
        public string TestPlanName
        {
            get => _testPlanName;
            set
            {
                _testPlanName = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Members
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Method of canceling (aborting) test plan
        /// </summary>
        private CancellationTokenSource _cancelSource = new();

        /// <summary>
        /// Current test plan selected through file path
        /// </summary>
        private TestPlan _testPlan;
        #endregion

        /// <summary>
        /// Will attempt to initialize OpenTAP and load plugins
        /// </summary>
        public MainViewModel()
        {
            try
            {
                // Load plugins and open a session log
                _ = PluginManager.SearchAsync();
                SessionLogs.Initialize("console_log.txt");
            }
            catch
            {
                throw;
            }
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new(propertyName));
        }

        /// <summary>
        /// Initiate test plan execution
        /// </summary>
        public async Task StartTestPlan()
        {
            if (IsRunning)
                return;
            if (!File.Exists(TapPlanFilePath))
                throw new Exception("Please select an existing tap plan");
            try
            {
                IsRunning = true;
                TestRunStatus = RunningStatus;
                // Load the test plan if not already loaded
                _testPlan ??= TestPlan.Load(TapPlanFilePath);
                // Use overload to pass in CancellationToken directly
                // Note: ExecuteAsync does NOT throw OperationCanceledException, execution just stops
                TestPlanRun planRun = await _testPlan.ExecuteAsync(new List<ResultListener>(), null, null, _cancelSource.Token);
                TestRunStatus = planRun.Verdict switch
                {
                    Verdict.Aborted => AbortedStatus,
                    Verdict.Fail => FailedStatus,
                    Verdict.Pass => PassedStatus,
                    Verdict.NotSet => CompletedStatus,
                    Verdict.Inconclusive => InconclusiveStatus,
                    Verdict.Error => ErrorStatus,
                    _ => NotRunningStatus
                };
            }
            catch
            {
                TestRunStatus = NotRunningStatus;
                throw;
            }
            finally
            {
                // A CancellationTokenSource can only be used once
                // Additional instantiation is required to cancel again
                _cancelSource = new();
                IsRunning = false;
            }
        }

        /// <summary>
        /// Abort the test plan (if running)
        /// </summary>
        public void AbortTestPlan()
        {
            if (NotRunning)
                return;
            try
            {
                _cancelSource.Cancel();
            }
            catch
            {
                throw;
            }
        }
    }
}
