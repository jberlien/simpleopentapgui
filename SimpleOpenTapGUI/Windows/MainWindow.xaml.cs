﻿using Microsoft.Win32;
using SimpleOpenTapGUI.ViewModels;
using System;
using System.Windows;

namespace SimpleOpenTapGUI.Windows
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainViewModel _viewModel;
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                _viewModel = new();
            }
            catch (Exception e)
            {
                _ = MessageBox.Show($"Error loading OpenTAP: {e.Message}\nApplication will shut down.", "CRITICAL ERROR");
                Application.Current.Shutdown();
            }
            DataContext = _viewModel;
        }

        /// <summary>
        /// Method "StartTestPlan" is awaited to catch exceptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Start_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await _viewModel.StartTestPlan();
            }
            catch (Exception exc)
            {
                _ = MessageBox.Show(exc.Message, "ERROR");
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _viewModel.AbortTestPlan();
            }
            catch (Exception exc)
            {
                _ = MessageBox.Show(exc.Message, "ERROR");
            }
        }

        private void Browse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dialog = new()
            {
                Title = "Select Tap Plan",
                Filter = "Tap Plans|*.tapplan"
            };
            bool? result = dialog.ShowDialog();
            // Do nothing if user pressed "Cancel"
            if (result.Value)
                _viewModel.TapPlanFilePath = dialog.FileName;
        }
    }
}
